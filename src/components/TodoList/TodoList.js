import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {removeTodo, fetchData} from '../../actions/todoListActions'

const Li = (props) => (
  <li>
    {props.i+1} - {props.text} 
    <button onClick={() => {props.re(props.id)}}>Usuń</button> 
  </li>
)

class TodoList extends React.Component{
  componentDidMount(){
    this.props.fetchData();
  }

  removeElement = (id) => {
    console.log(id)
    this.props.remove(id)
  }

  getTodos = () => {
    return this.props.todoList.todos.map((v,i) => (
      <Li re={this.removeElement} key={i} {...v} i={i} />
    ))
  }

  render(){
    return (
      <div>
        TODOLIST
        {this.props.todoList.loading 
          ? <p>Ładowanie...</p>
          : <ul>
              {this.getTodos()}
            </ul> 
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  todoList: state.todoList
})

const mapDispatchToProps = (dispatch) => ({
  remove: bindActionCreators(removeTodo, dispatch),
  fetchData: bindActionCreators(fetchData, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)