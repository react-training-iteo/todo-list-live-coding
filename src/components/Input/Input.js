import React from 'react';
import {connect} from 'react-redux'
import {addTodo} from '../../actions/todoListActions'

const Input = (props) => {
  const handleClick = () => {
    const text = this.input.value
    console.log(text)
    props.dispatch(addTodo(text))
    this.input.value = ''
  }

  return (
    <div>
      <input type="text" ref={(ref) => {this.input = ref}} />
      <button onClick={handleClick}>dodaj</button>
    </div>
  )
}

export default connect()(Input)