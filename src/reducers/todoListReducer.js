import * as types from '../constans/todoListConst'

const reducer = {
  [types.ADD_TODO]: (state, action) => {
    return {
      ...state,
      todos: [...state.todos, {text: action.payload, id: Math.random(), close:false}]
    }
  },
  [types.REMOVE_TODO]: (state, action) => {
    return {
      ...state,
      todos: state.todos.filter(v => v.id !== action.payload)
    }
  },
  [types.LOADING_TODO]: (state) => {
    return {
      ...state,
      loading: true
    }
  },
  [types.DONE_TODO]: (state, action) => {
    return {
      ...state,
      loading: false,
      todos: [...action.payload.data]
    }
  },
  [types.ERROR_TODO]: (state, action) => {
    return {
      ...state,
      loading: false,
      error: action.payload
    }
  }
}

const initState = {
  loading: false,
  error: null, 
  todos: [
    {id: 1, text: 'Zjeść śniadanie', close: false},
  ]
}

export default (state = initState, action) => {
  return reducer.hasOwnProperty(action.type)
    ? reducer[action.type](state, action)
    : state
}