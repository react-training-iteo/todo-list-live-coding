import * as types from '../constans/todoListConst';

export const addTodo = (text) => ({
  type: types.ADD_TODO,
  payload: text
})

export const removeTodo = (id) => ({
  type: types.REMOVE_TODO,
  payload: id
})

const loading = () => ({
  type: types.LOADING_TODO
}) 

const done = (data) => ({
  type: types.DONE_TODO,
  payload: {data}
})

const fail = (error) => ({
  type: types.ERROR_TODO,
  payload: error
})

export const fetchData = () => (dispatch) => {
  dispatch(loading());
  fetch('http://private-d417b-reactredux.apiary-mock.com/products').then(response => {
    response.json().then(data => {
      dispatch(done(data))
    })
  }).catch(err => {
    dispatch(fail(err))
  })
}